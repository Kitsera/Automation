package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

import org.hamcrest.Matchers;

import static org.junit.Assert.assertTrue;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class SearchStepDefinitions {
    EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();
    String baseUrl = EnvironmentSpecificConfiguration.from(variables).getProperty("webdriver.base.url");

    @When("we search for {word}")
    public void weSearchProduct(String product) {
        SerenityRest.given().get(baseUrl + "/" + product);
    }

    @Then("we receive {int} code")
    public void we_receive_code(Integer code) {
        restAssuredThat(response -> response.statusCode(code));
    }

    @Then("we {word} see list of items containg the {word}")
    public void we_see_list_of_items(String see, String text) {
        switch (see) {
            case "do":
                we_see_results_for_product(text);
                break;
            case "do_not":
                we_donot_see_results_for_product();
                break;
            default:
                assertTrue(false);
                break;
        }
    }

    @Then("we see results for the {word} product")
    public void we_see_results_for_product(String product) {
        restAssuredThat(response -> response.body("title", Matchers.containsStringIgnoringCase(product)));
    }

    @Then("we donot see results for the product")
    public void we_donot_see_results_for_product() {
        restAssuredThat(response -> response.body("detail.error", Matchers.equalTo(Boolean.TRUE)));
    }
}
Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

Scenario Outline: Search for a product
    When we search for <product>
    Then we receive <status> code
    And we <see> see list of items containg the <product>

    Examples:
        | product   | status  | see     |
        | orange    | 200     | do      |
        | apple     | 200     | do      |
        | pasta     | 200     | do      |
        | cola      | 200     | do      |
        | colapasta | 404     | do_not  |
        | car       | 404     | do_not  |
        | fanta     | 404     | do_not  |